import unittest
import json
import os
from checkov_transform import __main__ as checkov

# Path to current file
dirname = os.path.dirname(__file__) + "/"


class TestCheckovTransform(unittest.TestCase):

    def test_good_file_read(self):
        json_obj = checkov.load_json_file(dirname + "files/example_json.json")
        self.assertTrue("key" in json_obj)
        self.assertTrue(json_obj["key"] == "value")

    def test_bad_file_read(self):
        json_obj = checkov.load_json_file(dirname + "files/example_empty_json.json")
        self.assertTrue(not json_obj)

    def test_no_failures(self):
        json_string = checkov.main(dirname + "files/full_result_file_no_failure.json")
        json_obj = json.loads(json_string)

        vulnerabilities = len(json_obj["vulnerabilities"])
        self.assertTrue("version" in json_obj)
        self.assertTrue("vulnerabilities" in json_obj)
        self.assertEqual(vulnerabilities, 0)

    def test_failures(self):
        json_string = checkov.main(dirname + "files/full_result_file.json")
        json_obj = json.loads(json_string)

        vulnerabilities = len(json_obj["vulnerabilities"])
        self.assertTrue("version" in json_obj)
        self.assertTrue("vulnerabilities" in json_obj)
        self.assertEqual(vulnerabilities, 35)

    def test_failure_check_with_vulnerabilities(self):
        json_obj = checkov.load_json_file(dirname + "files/full_result_file.json")
        has_vulnerabilities = checkov.check_json_for_results(json_obj)
        self.assertTrue(has_vulnerabilities)

    def test_failure_check_without_vulnerabilities(self):
        json_obj = checkov.load_json_file(dirname + "files/full_result_file_no_failure.json")
        has_vulnerabilities = checkov.check_json_for_results(json_obj)
        self.assertFalse(has_vulnerabilities)

    def test_scan_results_with_multiple_scanners(self):
        json_string = checkov.main(dirname + "files/multiple_scan_results_file.json")
        json_obj = json.loads(json_string)

        vulnerabilities = len(json_obj["vulnerabilities"])
        self.assertTrue("version" in json_obj)
        self.assertTrue("vulnerabilities" in json_obj)
        self.assertGreater(vulnerabilities, 0)




if __name__ == "__main__":
    unittest.main()

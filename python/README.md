# Checkov GitLab Scanning Python module
This module performs the very simple function of translating the checkov JSON results
file into the GitLab AST security scan format, so that the results show up within the AST
results report within GitLab.